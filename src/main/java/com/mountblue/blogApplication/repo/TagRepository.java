package com.mountblue.blogApplication.repo;

import com.mountblue.blogApplication.entity.Blog;
import com.mountblue.blogApplication.entity.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TagRepository extends JpaRepository<Tag, Integer> {
    Tag findByTagName(String tagName);

}
