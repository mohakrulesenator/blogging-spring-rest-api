package com.mountblue.blogApplication.repo;

import com.mountblue.blogApplication.entity.Blog;
import com.mountblue.blogApplication.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BlogRepository extends JpaRepository<Blog, Integer> {

    public Page<Blog> findByIsPublishedTrue(Pageable pageable);
    public Page<Blog> findByIsPublishedTrueAndTitleLike(String search,Pageable pageable);
    public Page<Blog>  findByIsPublishedTrueAndContentLike(String search,Pageable page);
    @Query("select u from Blog u join u.tags t where u.isPublished=?1 and t.tagName=?2")
    public Page<Blog> findBytagsLike(boolean isPublished,String search,Pageable pageable);
//    @Query("select b from Blog b join fetch b.user where b.user = :user")
    public Page<Blog> findBlogByUser(User user,Pageable pageable);
    public Page<Blog> findBlogsByUserAndIsPublishedTrue(User user, Pageable pageable);
    public Page<Blog> findBlogsByUserAndIsPublishedFalse(User user,Pageable pageable);
    public void deleteAllByUser(User user);
    public Page<Blog> findByIsPublishedFalse(Pageable pageable);


    public List<Blog> findAllByIsPublishedTrue();
    public Blog findBlogById(int id);
    public Blog findBlogByIdAndIsPublishedTrue(int id);

}
