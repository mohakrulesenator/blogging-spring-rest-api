package com.mountblue.blogApplication.repo;

import com.mountblue.blogApplication.entity.Comment;
import com.mountblue.blogApplication.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentsRepository extends JpaRepository<Comment,Integer> {
    List<Comment> findByBlog_Id(int id);
    public void deleteAllByUser(User user);
    public Comment findCommentById(int id);
}
