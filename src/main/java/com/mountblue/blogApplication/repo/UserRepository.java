package com.mountblue.blogApplication.repo;

import com.mountblue.blogApplication.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface UserRepository extends JpaRepository<User,Integer> {

    Optional<User> findByUsername(String username);
    User findByemailIgnoreCase(String email);
    User findUserById(int id);

}
