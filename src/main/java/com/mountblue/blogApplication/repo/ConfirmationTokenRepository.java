package com.mountblue.blogApplication.repo;

import com.mountblue.blogApplication.entity.ConfirmationToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface ConfirmationTokenRepository extends JpaRepository<ConfirmationToken,Long> {
    ConfirmationToken findByconfirmationToken(String confirmationToken);
}
