package com.mountblue.blogApplication.restExceptionHandler;
import com.mountblue.blogApplication.exception.InvalidFormDataEnteredException;
import com.mountblue.blogApplication.message.Messages;
import com.mountblue.blogApplication.error.BlogErrorResponse;
import com.mountblue.blogApplication.exception.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@ControllerAdvice
public class BlogRestExceptionHandler {


    Date date = new Date();

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<BlogErrorResponse> handleException(EntityNotFoundException e) {
        BlogErrorResponse error = new BlogErrorResponse();
        error.setStatus(HttpStatus.NOT_FOUND.value());
        error.setMessage(e.getMessage());
        error.setTimeStamp(date);

        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<BlogErrorResponse> handleExceptionIllegalArgumentException(IllegalArgumentException e) {
        BlogErrorResponse error = new BlogErrorResponse();
        error.setStatus(HttpStatus.BAD_REQUEST.value());
        error.setMessage(e.getMessage());
        error.setTimeStamp(date);

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<BlogErrorResponse> handleException(InvalidFormDataEnteredException e) {
        BlogErrorResponse error = new BlogErrorResponse();
        error.setStatus(HttpStatus.BAD_REQUEST.value());
        error.setMessage(Messages.INVALID_FIELD_ARGUMENTS);
        error.setTimeStamp(date);

        return new ResponseEntity<>(error,HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<BlogErrorResponse> handleException(Exception e) {
        BlogErrorResponse error = new BlogErrorResponse();
        error.setStatus(HttpStatus.BAD_REQUEST.value());
        error.setMessage(Messages.INVALID_ARGUMENT);
        error.setTimeStamp(date);

        return new ResponseEntity<>(error,HttpStatus.BAD_REQUEST);
    }

}
