package com.mountblue.blogApplication.exception;

public class InvalidFormDataEnteredException extends RuntimeException {
    public InvalidFormDataEnteredException(String message) {
        super(message);
    }

    public InvalidFormDataEnteredException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidFormDataEnteredException(Throwable cause) {
        super(cause);
    }
}
