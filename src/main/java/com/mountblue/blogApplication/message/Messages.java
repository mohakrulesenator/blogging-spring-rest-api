package com.mountblue.blogApplication.message;

public class Messages {
    public static final String INVALID_ARGUMENT = "Expected argument as int but provided different argument.";
    public static final String INVALID_BLOG_ID = "Invalid blog Id provided.";
    public static final String BLOG_NOT_FOUND = "Blog not found having id ";
    public static final String COMMENT_NOT_FOUND = "Comment not found having id ";
    public static final String INVALID_USER_ID = "Invalid user Id provided.";
    public static final String USER_NOT_FOUND = "User not found having id ";
    public static final String INVALID_COMMENT_ID = "Invalid comment Id provided.";
    public static final String INVALID_FIELD_ARGUMENTS = "Invalid Data entered.";
    public static final String UNAUTHORIZED_ACCESS = "Forbidden.Unauthorized access";

    public static final String VALID_REQUEST = "Request processed successfully.";
}
