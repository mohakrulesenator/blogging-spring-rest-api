package com.mountblue.blogApplication.service;

import com.mountblue.blogApplication.entity.User;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Component
public interface UserService {

    public Optional<User> findByUsername(String username);

    public List<User> findAll();

    public User findById(int theId);

    public void save(User theUser);

    public void deleteById(int theId);

    public User findByemailIgnoreCase(String email);

    User findUserById(int id);

}
