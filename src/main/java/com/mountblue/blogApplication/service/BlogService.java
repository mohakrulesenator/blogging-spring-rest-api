package com.mountblue.blogApplication.service;

import com.mountblue.blogApplication.entity.Blog;
import com.mountblue.blogApplication.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;


public interface BlogService {

    public List<Blog> findAll();

    public Blog findById(int theId);

    public void save(Blog theBlog);

    public void deleteById(int theId);

    public Page<Blog> listAllBlogs(int page,int size,String sortBy, String orderBy);

    public Page<Blog> listAllBlogsByTitle(String search, int page,int size);

    public Page<Blog>  listAllBlogsByContent(String search, int page,int size);

    public Page<Blog> listAllBlogsByTags(boolean isPublished,String search,int page,int size);

    public Page<Blog> findBlogsByUserAndIsPublishedTrue(User user,int page,int size,String sortBy,String orderBy);

    public Page<Blog> findBlogsByUserAndIsPublishedFalse(User user,int page,int size,String sortBy,String orderBy);

    public Page<Blog> findBlogByUser(User user,int page);

    public void deleteAllByUser(User user);

    public Page<Blog> listAllDrafts(int page);

    public List<Blog> findAllByIsPublishedTrue();

    public Blog findBlogById(int id);

    public Blog findBlogByIdAndIsPublishedTrue(int id);


}
