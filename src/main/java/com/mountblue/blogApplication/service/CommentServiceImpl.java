package com.mountblue.blogApplication.service;

import com.mountblue.blogApplication.entity.Comment;
import com.mountblue.blogApplication.entity.User;
import com.mountblue.blogApplication.repo.CommentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentsRepository commentRepo;

    @Override
    public List<Comment> findAll() {
        return (List<Comment>)commentRepo.findAll();
    }

    @Override
    public Comment findById(int theId) {

        Optional<Comment> result = commentRepo.findById(theId);

        Comment comment = null;

        if (result.isPresent()) {
            comment = result.get();
        }
        else {
            throw new RuntimeException("Comment with this id - " + theId + "is not found");
        }

        return comment;
    }

    @Override
    public void save(Comment theComment) {
        commentRepo.save(theComment);
    }

    @Override
    public void deleteById(int theId) {
        commentRepo.deleteById(theId);
    }

    @Override
    public List<Comment> findByPost_Id(int id) {
        return commentRepo.findByBlog_Id(id);
    }

    @Override
    public void deleteAllByUser(User user) {
        commentRepo.deleteAllByUser(user);
    }

    @Override
    public Comment findCommentById(int id) {
        return commentRepo.findCommentById(id);
    }


}
