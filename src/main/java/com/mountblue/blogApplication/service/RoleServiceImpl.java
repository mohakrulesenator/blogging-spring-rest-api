package com.mountblue.blogApplication.service;

import com.mountblue.blogApplication.entity.Role;
import com.mountblue.blogApplication.repo.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleRepository roleRepo;

    @Override
    public List<Role> findAll() {
        return roleRepo.findAll();
    }

    @Override
    public void save(Role role) {
        roleRepo.save(role);
    }

    @Override
    public Role findByRole(String role) {
        return roleRepo.findByRole(role);
    }
}
