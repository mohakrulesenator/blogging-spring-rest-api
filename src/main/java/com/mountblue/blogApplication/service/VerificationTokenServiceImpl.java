package com.mountblue.blogApplication.service;

import com.mountblue.blogApplication.entity.User;
import com.mountblue.blogApplication.entity.VerificationTokenClass;
import com.mountblue.blogApplication.repo.VerificationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VerificationTokenServiceImpl implements VerificationTokenService {

    @Autowired
    VerificationTokenRepository verificationTokenRepo;

    @Override
    public void save(VerificationTokenClass verificationToken) {
        verificationTokenRepo.save(verificationToken);
    }

    @Override
    public void deleteById(long theId) {
        verificationTokenRepo.deleteById(theId);

    }

    @Override
    public VerificationTokenClass findByUser(User user) {
        return verificationTokenRepo.findByUser(user);
    }

    @Override
    public VerificationTokenClass findByVerificationToken(String verificationToken) {
        return verificationTokenRepo.findByVerificationToken(verificationToken);
    }

    @Override
    public void deleteByVerificationTokenId(long id) {
        verificationTokenRepo.deleteByVerificationTokenId(id);
    }


}
