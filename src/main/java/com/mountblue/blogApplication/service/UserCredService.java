package com.mountblue.blogApplication.service;

import com.mountblue.blogApplication.entity.User;
import com.mountblue.blogApplication.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserCredService implements UserDetailsService,UserService {

    @Autowired
    UserRepository userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepo.findByUsername(username);

        user.orElseThrow(() -> new UsernameNotFoundException("Not found" + username));

        return user.map(UserCred::new).get();
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return userRepo.findByUsername(username);
    }

    @Override
    public List<User> findAll() {
        return (List<User>) userRepo.findAll();
    }
    @Override
    public User findById(int theId) {
        Optional<User> result = userRepo.findById(theId);

        User theUser = null;

        if (result.isPresent()) {
            theUser = result.get();
        }
        else {
            throw new RuntimeException("User with this id - " + theId + "is not found");
        }

        return theUser;
    }

    @Override
    public void save(User theUser) {
        userRepo.save(theUser);
    }
    @Override
    public void deleteById(int theId) {
        userRepo.deleteById(theId);
    }

    @Override
    public User findByemailIgnoreCase(String email) {
        return userRepo.findByemailIgnoreCase(email);
    }

    @Override
    public User findUserById(int id) {
        return userRepo.findUserById(id);
    }


}
