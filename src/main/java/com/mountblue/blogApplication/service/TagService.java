package com.mountblue.blogApplication.service;

import com.mountblue.blogApplication.entity.Blog;
import com.mountblue.blogApplication.entity.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Component
public interface TagService {

    public List<Tag> findAll();

    public Tag findById(int theId);

    public Tag findByTagName(String tagName);

    public void save(Tag theTag);

    public void deleteById(int theId);




}
