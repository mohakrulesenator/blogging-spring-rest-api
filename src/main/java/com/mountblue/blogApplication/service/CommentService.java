package com.mountblue.blogApplication.service;

import com.mountblue.blogApplication.entity.Blog;
import com.mountblue.blogApplication.entity.Comment;
import com.mountblue.blogApplication.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;


public interface CommentService {

    public List<Comment> findAll();

    public Comment findById(int theId);

    public void save(Comment theComment);

    public void deleteById(int theId);

    public List<Comment> findByPost_Id(int id);

    public void deleteAllByUser(User user);

    public Comment findCommentById(int id);

}


