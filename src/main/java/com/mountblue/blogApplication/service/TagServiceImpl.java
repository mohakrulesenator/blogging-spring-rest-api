package com.mountblue.blogApplication.service;

import com.mountblue.blogApplication.entity.Blog;
import com.mountblue.blogApplication.entity.Tag;
import com.mountblue.blogApplication.repo.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TagServiceImpl implements TagService {
    @Autowired
    private TagRepository tagRepo;

    @Override
    public List<Tag> findAll() {
        return (List<Tag>)tagRepo.findAll();
    }

    @Override
    public Tag findById(int theId) {
        Optional<Tag> result = tagRepo.findById(theId);

        Tag theTag = null;

        if (result.isPresent()) {
            theTag = result.get();
        }
        else {
            throw new RuntimeException("Tag with this id - " + theId + "is not found");
        }

        return theTag;
    }

    @Override
    public Tag findByTagName(String tagName) {
        return tagRepo.findByTagName(tagName);
    }

    @Override
    public void save(Tag theTag) {
        tagRepo.save(theTag);
    }

    @Override
    public void deleteById(int theId) {
        tagRepo.deleteById(theId);
    }


}
