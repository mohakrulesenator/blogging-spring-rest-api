package com.mountblue.blogApplication.service;

import com.mountblue.blogApplication.api.UserController;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class EmailSenderService {

    Logger logger = LoggerFactory.getLogger(UserController.class);

    @Value("${app.sendgrid.key}")
    private String key;

    public void sendEmail(String email,String subject, String body) throws IOException {
        Email from = new Email("mohak.bhatnagar@mountblue.tech");
        Email to = new Email(email);
        Content content = new Content("text/plain",body);
        Mail mail = new Mail(from,subject,to,content);

        SendGrid sg = new SendGrid(key);
        Request request = new Request();
        try{
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
        } catch(IOException e) {
            logger.error("Error in sending mail.");
            e.printStackTrace();
        }
        logger.info("MAIL SENT PLEASE CHECK YOUR ACCOUNT");
    }

}
