package com.mountblue.blogApplication.service;

import com.mountblue.blogApplication.entity.User;
import com.mountblue.blogApplication.entity.VerificationTokenClass;


public interface VerificationTokenService {

    public void save(VerificationTokenClass verificationToken);

    public void deleteById(long theId);

    public VerificationTokenClass findByUser(User user);

    VerificationTokenClass findByVerificationToken(String verificationToken);

    public void deleteByVerificationTokenId(long id);
}
