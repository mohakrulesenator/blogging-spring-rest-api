package com.mountblue.blogApplication.entity;

//import org.mindrot.jbcrypt.BCrypt;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.*;
import javax.validation.constraints.Email;

@Entity
@Table(name="USERS")
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private int id;

    @NotNull
    @Size(min=3,message="NAME SHOULD HAVE LENGTH GREATER THAN 3.")
    @Column(name="FIRST_NAME")
    private String firstName;

    @NotNull
    @Size(min=3,message="NAME SHOULD HAVE LENGTH GREATER THAN 3.")
    @Column(name="LAST_NAME")
    private String lastName;

    @NotNull
    @Pattern(regexp = "[6-9][0-9]{9}",message = "INVALID PHONE NO")
    @Size(max=10,message = "PHONE NO SHOULD BE OF LENGTH 10 ")
    @Column(name="CONTACT")
    private String contact;

    @NotNull
    @Email(regexp = "[a-zA-Z0-9.-_]+@[a-zA-Z-]+\\.[a-z]+",message = "ENTER VALID EMAIL ADDRESS.")
    @Column(name="EMAIL")
    private String email;

    @NotNull
    @Size(min=3,message="USERNAME SHOULD HAVE LENGTH GREATER THAN 3.")
    @Column(name="USERNAME")
    private String username;

    @NotBlank
    @Column(name="PASSWORD")
    private String password;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="ROLE_ID")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Role role;

    @Column(name="IS_ACTIVE")
    private boolean active;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }


}
