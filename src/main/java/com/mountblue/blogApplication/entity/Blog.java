package com.mountblue.blogApplication.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name="POSTS")
public class Blog {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="BLOG_ID")
    private int id;

    @NotBlank(message = "TITLE FIELD CANNOT BE EMPTY.PLEASE TRY AGAIN.")
    @Column(name="TITLE")
    private String title;

    @NotBlank(message = "SUBJECT FIELD CANNOT BE EMPTY.PLEASE TRY AGAIN.")
    @Column(name="SUBJECT")
    private String subject;

    @NotBlank(message = "CONTENT FIELD CANNOT BE EMPTY.PLEASE TRY AGAIN.")
    @Column(name="CONTENT",columnDefinition = "TEXT")
    private String content;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="PUBLISHED_ON")
    @UpdateTimestamp
    private Date publishedOn;

    @Column(name="IS_PUBLISHED")
    private boolean isPublished;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATED_ON",updatable = false)
    @CreationTimestamp
    private Date createdOn;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="UPDATED_ON")
    @UpdateTimestamp
    private Date updatedOn;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="author_id")
    private User user;

    @Transient
    private String blogTags;


    @ManyToMany(fetch=FetchType.LAZY,cascade={CascadeType.MERGE,CascadeType.PERSIST})
    @JoinTable(
            name="blog_tags",
            joinColumns=@JoinColumn(name="BLOG_ID"),inverseJoinColumns = @JoinColumn(name="TAG_ID")
    )
    private Set<Tag> tags = new HashSet<>();

    public boolean isPublished() {
        return isPublished;
    }

    public void setPublished(boolean published) {
        isPublished = published;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }





    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }


    public boolean isIsPublished() {
        return isPublished;
    }

    public void setIsPublished(boolean is_published) {
        this.isPublished = is_published;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date created_on) {
        this.createdOn = created_on;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updated_on) {
        this.updatedOn = updated_on;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public Date getPublishedOn() {
        return publishedOn;
    }

    public void setPublishedOn(Date published_on) {
        this.publishedOn = published_on;
    }

    public String getBlogTags() {
        return blogTags;
    }

    public void setBlogTags(String blogTags) {
        this.blogTags = blogTags;
    }







}
