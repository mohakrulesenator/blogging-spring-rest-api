package com.mountblue.blogApplication.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "TAGS")
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="TAG_ID")
    private int id;

    @Column(name="TAG_NAME")
    private String tagName;

    @JsonIgnore
    @Column(name="CREATED_AT")
    @CreationTimestamp
    private Date created_at;

    @JsonIgnore
    @Column(name="UPDATED_AT")
    @UpdateTimestamp
    private Date updated_at;



    @JsonIgnore
    @ManyToMany(fetch=FetchType.LAZY,cascade={CascadeType.PERSIST},mappedBy="tags")
    private Set<Blog> blogs = new HashSet<>();

    public Set<Blog> getBlogs() {
        return blogs;
    }

    public void setBlogs(Set<Blog> blogs) {
        this.blogs = blogs;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }



}
