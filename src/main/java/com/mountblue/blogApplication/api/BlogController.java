package com.mountblue.blogApplication.api;

import com.mountblue.blogApplication.entity.Blog;
import com.mountblue.blogApplication.entity.Tag;
import com.mountblue.blogApplication.entity.User;
import com.mountblue.blogApplication.exception.EntityNotFoundException;
import com.mountblue.blogApplication.exception.InvalidFormDataEnteredException;
import com.mountblue.blogApplication.message.Messages;
import com.mountblue.blogApplication.service.BlogService;
import com.mountblue.blogApplication.service.CommentServiceImpl;
import com.mountblue.blogApplication.service.TagServiceImpl;
import com.mountblue.blogApplication.service.UserCredService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api")
@RestController
public class BlogController {

    Logger logger = LoggerFactory.getLogger(BlogController.class);

    @Autowired
    private BlogService blogService;

    @Autowired
    UserCredService userServiceImpl;

    @Autowired
    TagServiceImpl tagService;

    @Autowired
    private CommentServiceImpl commentService;

    @GetMapping("/blogs")
    public ResponseEntity<?> getAllBlogs(@RequestParam(value="start",defaultValue = "0") int start, @RequestParam(value="size",defaultValue = "2")int size,
                                         @RequestParam(value="sortBy",defaultValue = "createdOn")String sortBy,
                                         @RequestParam(value="orderBy",defaultValue = "desc")String orderBy) {
        Page<Blog> list = blogService.listAllBlogs(start,size,sortBy,orderBy);
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/blogs/{blogId}")
    public ResponseEntity<?> getBlog(@PathVariable int blogId) {
        if(blogId < 1) {
            throw new IllegalArgumentException(Messages.INVALID_BLOG_ID);
        }
        Blog blog = blogService.findBlogByIdAndIsPublishedTrue(blogId);
        if(blog == null) {
            throw new EntityNotFoundException(Messages.BLOG_NOT_FOUND+blogId);
        }
        return ResponseEntity.status(200).body(blog);
    }

    @PostMapping("/blogs")
    public ResponseEntity<?> addBlog(@Valid @RequestBody Blog blog, Errors errors,@RequestParam(value="saveAsDraft",defaultValue = "false") boolean saveAsDraft, Authentication authentication) {

        if(errors.hasErrors()) {
            logger.error("NOT A VALID POST");
            throw new InvalidFormDataEnteredException(Messages.INVALID_FIELD_ARGUMENTS);
        }
        if(authentication == null) {
            logger.error("UNAUTHORIZED ACCESS");
            return ResponseEntity.status(403).body(Messages.UNAUTHORIZED_ACCESS);
        }

        User user = userServiceImpl.findByUsername(authentication.getName()).get();
        blog.setUser(user);
        blog.setIsPublished(!saveAsDraft);
        logger.info("SAVING TAGS");
        String allTags[] = blog.getBlogTags().split(",");
        for (String tag : allTags) {
            Tag oldTag = tagService.findByTagName(tag);
            if (oldTag == null) {
                Tag newTag = new Tag();
                newTag.setTagName(tag);
                blog.getTags().add(newTag);
                newTag.getBlogs().add(blog);
            } else {
                oldTag.getBlogs().add(blog);
                blog.getTags().add(oldTag);
            }
        }
        blogService.save(blog);
        return ResponseEntity.status(200).body(Messages.VALID_REQUEST);
    }

    @PutMapping("/blogs/{blogId}")
    public ResponseEntity<?> updateBlog(@Valid @RequestBody Blog blog, BindingResult result, @PathVariable int blogId, Authentication authentication) {
        if(authentication == null) {
            return ResponseEntity.status(403).body(Messages.UNAUTHORIZED_ACCESS);
        }
        if(result.hasErrors()) {
            logger.error("NOT A VALID POST");
            throw new InvalidFormDataEnteredException(Messages.INVALID_FIELD_ARGUMENTS);
        }
        if(blogId < 1) {
            throw new IllegalArgumentException(Messages.INVALID_BLOG_ID);
        }
        User user = blogService.findBlogById(blogId).getUser();
        if(user.getUsername().equals(authentication.getName()) || authentication.getAuthorities().toString().contains("ROLE_ADMIN")) {
            blog.setId(blogId);
            blog.setIsPublished(true);
            blog.setUser(user);
            if(blog.getBlogTags() != null) {
                String allTags[] = null;
                allTags = blog.getBlogTags().split(",");
                for (String tag : allTags) {
                    Tag oldTag = tagService.findByTagName(tag);
                    if (oldTag == null) {
                        Tag newTag = new Tag();
                        newTag.setTagName(tag);
                        blog.getTags().add(newTag);
                        newTag.getBlogs().add(blog);
                    } else {
                        oldTag.getBlogs().add(blog);
                        blog.getTags().add(oldTag);
                    }
                }
            }
            blogService.save(blog);
        }
        else{
            return ResponseEntity.status(403).body(Messages.UNAUTHORIZED_ACCESS);
        }
        return ResponseEntity.status(200).body(Messages.VALID_REQUEST);
    }

    @DeleteMapping("/blogs/{blogId}")
    public ResponseEntity<?> deleteBlog(@PathVariable int blogId,Authentication authentication) {
        if(authentication == null) {
            return ResponseEntity.status(403).body(Messages.UNAUTHORIZED_ACCESS);
        }
        if(blogId < 1) {
            throw new IllegalArgumentException(Messages.INVALID_BLOG_ID);
        }
        Blog blog = blogService.findBlogByIdAndIsPublishedTrue(blogId);
        if(blog == null) {
            throw new EntityNotFoundException(Messages.BLOG_NOT_FOUND+blogId);
        }
        User user = userServiceImpl.findByUsername(blog.getUser().getUsername()).get();
        if(user.getUsername().equals(authentication.getName()) || authentication.getAuthorities().toString().equals("ROLE_ADMIN")) {
            blogService.deleteById(blogId);
            return ResponseEntity.status(204).body(Messages.VALID_REQUEST);
        }else {
            return ResponseEntity.status(403).body(Messages.UNAUTHORIZED_ACCESS);
        }
    }

    @GetMapping("/search/{field}/{keyword}")
    public ResponseEntity<?> search(@PathVariable String field,@PathVariable String keyword,@RequestParam(value="start",defaultValue = "0") int page,
                                    @RequestParam(value="size",defaultValue = "2")int size,
                                    @RequestParam(value="sortBy",defaultValue = "publishedOn")String sortBy,
                                    @RequestParam(value="orderBy",defaultValue="desc")String orderBy) {

        if(field.contains("author")) {
            User author = userServiceImpl.findByUsername(keyword).get();
            Page<Blog> list = blogService.findBlogsByUserAndIsPublishedTrue(author,page,size,sortBy,orderBy);
            return ResponseEntity.status(200).body(list);
        }
        else if(field.contains("tags")) {
            Page<Blog> list = blogService.listAllBlogsByTags(true,keyword,page,size);
            return ResponseEntity.status(200).body(list);
        }
        else if(field.contains("content")) {
            keyword += "%"+keyword+"%";
            Page<Blog> list = blogService.listAllBlogsByContent(keyword,page,size);
            return ResponseEntity.status(200).body(list);
        }
        else if(field.contains("title")) {
            Page<Blog> list = blogService.listAllBlogsByTitle(keyword,page,size);
            return ResponseEntity.status(200).body(list);
        }
        return ResponseEntity.badRequest().body("INVALID KEYWORD");
    }

    @GetMapping("/filter/{tagname}")
    public ResponseEntity<?> filterByTagName(@PathVariable String tagname,@RequestParam(value="start",defaultValue = "0")int start,
                                             @RequestParam(value = "size",defaultValue = "2")int size) {

        Tag tag = tagService.findByTagName(tagname);
        Page<Blog> list = blogService.listAllBlogsByTags(true,tag.getTagName(),start,size);
        return ResponseEntity.status(200).body(list);
    }




}
