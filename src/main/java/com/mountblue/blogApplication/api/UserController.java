package com.mountblue.blogApplication.api;

import com.mountblue.blogApplication.entity.*;
import com.mountblue.blogApplication.exception.EntityNotFoundException;
import com.mountblue.blogApplication.message.Messages;
import com.mountblue.blogApplication.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
@RestController
@RequestMapping("/api")
public class UserController {

    Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private BlogService blogService;

    @Autowired
    UserCredService userServiceImpl;

    @Autowired
    TagServiceImpl tagService;

    @Autowired
    private CommentServiceImpl commentService;

    @Autowired
    private RoleServiceImpl roleService;

    @Autowired
    private ConfirmationTokenServiceImpl confirmationTokenService;

    @Autowired
    private VerificationTokenServiceImpl verificationTokenService;

    @Autowired
    EmailSenderService mailSender;

    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10);



    @GetMapping("/users/{userId}/blogs")//userFeed
    public ResponseEntity<?> getAllBlogsOfUser(@PathVariable("userId") int id,Authentication authentication,@RequestParam(value="start",defaultValue = "0") int page,
                                               @RequestParam(value="size",defaultValue = "2")int size,
                                               @RequestParam(value="sortBy",defaultValue = "publishedOn")String sortBy,
                                               @RequestParam(value="orderBy",defaultValue="desc")String orderBy,
                                               @RequestParam(value="show-drafts",defaultValue = "false")boolean showDrafts) {
        System.out.println(authentication.getName());
        if(id < 1) {
            throw new IllegalArgumentException(Messages.INVALID_USER_ID);
        }
        if(authentication == null ) {
            return ResponseEntity.status(403).body("FORBIDDEN.UNAUTHORIZED ACCESS.");
        }
        User author = userServiceImpl.findUserById(id);
        if(author == null) {
            throw new EntityNotFoundException(Messages.USER_NOT_FOUND+id);
        }
        if(authentication.getName().equals(author.getUsername()) || authentication.getAuthorities().toString().contains("ROLE_ADMIN")) {
            Page<Blog> list = null;
            if(showDrafts) {
                list = blogService.findBlogsByUserAndIsPublishedFalse(author, page,size,sortBy,orderBy);
            }
            else {
                list = blogService.findBlogsByUserAndIsPublishedTrue(author, page, size,sortBy,orderBy);
            }
            return ResponseEntity.status(200).body(list);
        }
        else{
            return ResponseEntity.status(403).body("FORBIDDEN.UNAUTHORIZED ACCESS.");
        }

    }

    @GetMapping("/users")
    public ResponseEntity<?> getAllUsers(Authentication authentication) {
        logger.info(authentication.getAuthorities().toString());
        if(authentication.getAuthorities().toString().equals("ROLE_ADMIN")) {
            List<User> allUsers = userServiceImpl.findAll();
            return ResponseEntity.status(200).body(allUsers);
        }
        else {
            return ResponseEntity.status(403).body("FORBIDDEN. UNAUTHORIZED ACCESS");
        }
    }


    @PostMapping("/users")
    public ResponseEntity<?> AddUser(@Valid @RequestBody User user, BindingResult result, HttpServletRequest request) throws IOException {
        System.out.println(user.getFirstName());
        if(result.hasErrors()) {
            System.out.println("CHECK");
            System.out.println(result.getAllErrors());
            return ResponseEntity.badRequest().body("Invalid user details. Please try again");
        }

        User existingUser = userServiceImpl.findByemailIgnoreCase(user.getEmail());
        if(existingUser != null) {
            logger.error("USER ALREADY EXISTS");
            return ResponseEntity.status(403).body("USER ALREADY EXISTS. PLEASE TRY AGAIN");
        }
        else {
            System.out.println(user.getUsername());
            ConfirmationToken confirmationToken = new ConfirmationToken(user);
            logger.info(confirmationToken.getConfirmationToken());
            logger.info(confirmationToken.getUser().getUsername());
            confirmationTokenService.save(confirmationToken);
            String subject = "Complete Registration !";
            String body = "To confirm your account, please click here : "
                    + "http://" + request.getServerName() + ":" + request.getServerPort() + "/api/confirm-account?token=" + confirmationToken.getConfirmationToken();
            mailSender.sendEmail(user.getEmail(),subject,body);
            return ResponseEntity.status(200).body("MAIL SENT PLEASE CHECK YOUR INBOX");
        }

    }

    @GetMapping("/confirm-account")
    public ResponseEntity<?> confirmUserAccount(@RequestParam("token")String confirmationToken) {

        System.out.println(confirmationToken);
        ConfirmationToken token = confirmationTokenService.findByconfirmationToken(confirmationToken);

        if(token != null) {
            User user = userServiceImpl.findByemailIgnoreCase(token.getUser().getEmail());
            System.out.println(user.getUsername());
            user.setActive(true);
            user.setPassword(encoder.encode(user.getPassword()));
            Role role = roleService.findByRole("ROLE_USER");
            user.setRole(role);
            userServiceImpl.save(user);
            logger.info("USER VERIFIED");
            long token_id = token.getTokenid();
            confirmationTokenService.deleteById(token_id);
        }
        else{
            logger.error("INVALID USER");
            return ResponseEntity.badRequest().body("INVALID USER");
        }
        return ResponseEntity.status(200).body("USER CREATED SUCCESSFULLY");
    }

    @PostMapping("/forgotPassword")
    public ResponseEntity<String> sendVerificationToken(@RequestBody Email email, HttpServletRequest request, Authentication authentication) throws IOException {
        if(authentication != null) {
            return ResponseEntity.badRequest().body("USER ALREADY LOGGED IN.");
        }
        logger.info(email.getEmail());
        User user = userServiceImpl.findByemailIgnoreCase(email.getEmail());
        if(user != null) {
            logger.info(user.getUsername());
            VerificationTokenClass verificationToken = new VerificationTokenClass(user);
            verificationTokenService.save(verificationToken);
            String subject = "Complete Password reset !";
            String body = "To reset your password, please click here : "
                    + "http://" + request.getServerName() + ":" + request.getServerPort() + "/api/confirm-reset?token=" + verificationToken.getVerificationToken();
            mailSender.sendEmail(user.getEmail(),subject,body);
            return ResponseEntity.status(200).body("RESET PASSWORD MAIL SENT PLEASE CHECK YOUR INBOX");
        }
        else{
            return ResponseEntity.status(404).body("USER NOT FOUND.PLEASE REGISTER");
        }
    }

    @GetMapping("/confirm-reset")
    public ResponseEntity<?> resetPassword(@RequestParam(value="token")String verificationToken){
        VerificationTokenClass token = verificationTokenService.findByVerificationToken(verificationToken);
        if(token != null) {
            User user = token.getUser();
            long token_id = token.getVerificationTokenId();
            verificationTokenService.deleteById(token_id);
            user.setActive(true);
            userServiceImpl.save(user);
            return ResponseEntity.status(200).body("Password token verified successfully");
        }
        else{
            logger.error("This link is invalid or broken");
            return ResponseEntity.status(403).body("FORBIDDEN");
        }

    }

    @PutMapping("/resetPassword/{email}")
    public ResponseEntity<?> resetUser(@PathVariable String email, @RequestBody ResetPassword resetPassword) {
        User tokenUser = userServiceImpl.findByemailIgnoreCase(email);
        tokenUser.setPassword(encoder.encode(resetPassword.getPassword()));
        userServiceImpl.save(tokenUser);
        return ResponseEntity.status(200).body("PASSWORD RESET SUCCESS FOR ACCOUNT "+tokenUser.getUsername());
    }

    @Transactional
    @DeleteMapping("/users/{userId}")
    public ResponseEntity<?> deleteUser(@PathVariable int userId,Authentication authentication) {
        if(authentication.getAuthorities().toString().contains("ROLE_ADMIN")) {
            User user = null;
            try{
                user = userServiceImpl.findById(userId);
            }catch (Exception e) {
                return ResponseEntity.status(404).body("USER NOT FOUND");
            }
            if(user == null) {
                return ResponseEntity.status(404).body("USER NOT FOUND");
            }
            if(user.getUsername().equals(authentication.getName())){
                return ResponseEntity.badRequest().body("CANNOT DELETE USER WHILE LOGGED IN");
            }
            commentService.deleteAllByUser(user);
            blogService.deleteAllByUser(user);
            userServiceImpl.deleteById(userId);
            return ResponseEntity.status(200).body("DELETED USER SUCCESSFULLY");
        }
        return ResponseEntity.status(403).body("FORBIDDEN.UNAUTHORIZED ACCESS.");
    }

}
