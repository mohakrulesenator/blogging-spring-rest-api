package com.mountblue.blogApplication.api;


import com.mountblue.blogApplication.entity.Blog;
import com.mountblue.blogApplication.entity.Comment;
import com.mountblue.blogApplication.entity.User;
import com.mountblue.blogApplication.exception.InvalidFormDataEnteredException;
import com.mountblue.blogApplication.exception.EntityNotFoundException;
import com.mountblue.blogApplication.message.Messages;
import com.mountblue.blogApplication.service.BlogService;
import com.mountblue.blogApplication.service.CommentServiceImpl;
import com.mountblue.blogApplication.service.TagServiceImpl;
import com.mountblue.blogApplication.service.UserCredService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CommentController {

    Logger logger = LoggerFactory.getLogger(CommentController.class);

    @Autowired
    private BlogService blogService;

    @Autowired
    UserCredService userServiceImpl;

    @Autowired
    TagServiceImpl tagService;

    @Autowired
    private CommentServiceImpl commentService;

    @GetMapping("/blogs/{blogId}/comments")
    public ResponseEntity<?> getAllComments(@PathVariable int blogId) {
        if(blogId < 1) {
            throw new IllegalArgumentException(Messages.INVALID_BLOG_ID);
        }
        Blog blog = blogService.findBlogByIdAndIsPublishedTrue(blogId);
        if(blog == null) {
            throw new EntityNotFoundException(Messages.BLOG_NOT_FOUND+blogId);
        }
        List<Comment> list = commentService.findByPost_Id(blogId);
        return ResponseEntity.status(200).body(list);
    }

    @PostMapping("/blogs/{blogId}/comments")
    public ResponseEntity<?> addComment(@PathVariable int blogId, @Valid @RequestBody Comment comment, BindingResult errors, Authentication authentication) {
        if(blogId < 1) {
            logger.error("INVALID BLOG ID.");
            throw new IllegalArgumentException(Messages.INVALID_BLOG_ID);
        }
        Blog blog = blogService.findBlogByIdAndIsPublishedTrue(blogId);
        if(blog == null) {
            logger.error("NO BLOG FOUND.");
            throw new EntityNotFoundException(Messages.BLOG_NOT_FOUND+blogId);
        }
        if(errors.hasErrors()) {
            logger.error("ERROR IN COMMENT DETAILS.");
            throw new InvalidFormDataEnteredException(Messages.INVALID_FIELD_ARGUMENTS);
        }

        User user = userServiceImpl.findByUsername(authentication.getName()).get();

        comment.setBlog(blog);
        comment.setUser(user);
        commentService.save(comment);
        return ResponseEntity.status(200).body("COMMENT ADDED FOR BLOG ID "+blog.getId());
    }

    @PutMapping("/blogs/{blogId}/comments/{commentId}")
    public ResponseEntity<?> updateComment(@PathVariable int blogId,@RequestBody Comment comment,BindingResult result,@PathVariable int commentId,Authentication authentication) {
        if(result.hasErrors()) {
            logger.error("INVALID FIELD VALUES");
            throw new InvalidFormDataEnteredException(Messages.INVALID_FIELD_ARGUMENTS);
        }
        if(authentication == null) {
            logger.warn("UNAUTHORIZED ACCESS");
            return ResponseEntity.status(403).body(Messages.UNAUTHORIZED_ACCESS);
        }
        if(blogId < 1) {
            logger.error("INVALID BLOG ID.");
            throw new IllegalArgumentException(Messages.INVALID_BLOG_ID);
        }
        if(commentId < 1) {
            throw new IllegalArgumentException(Messages.INVALID_COMMENT_ID);
        }
        Blog blog = blogService.findBlogByIdAndIsPublishedTrue(blogId);
        if(blog == null) {
            throw new EntityNotFoundException(Messages.BLOG_NOT_FOUND+blogId);
        }
        Comment commentStored = commentService.findCommentById(commentId);
        if(commentStored == null) {
            throw new EntityNotFoundException(Messages.COMMENT_NOT_FOUND+commentId);
        }
        User commentAuthor = commentStored.getUser();
        if(authentication.getName().equals(commentAuthor.getUsername()) || authentication.getAuthorities().toString().contains("ROLE_ADMIN")) {
            comment.setId(commentId);
            comment.setUser(commentAuthor);
            comment.setBlog(blog);
            commentService.save(comment);
        }
        else{
            return ResponseEntity.status(403).body(Messages.UNAUTHORIZED_ACCESS);
        }
        return ResponseEntity.status(200).body("COMMENT UPDATED "+"HAVING ID"+comment.getId());
    }

    @DeleteMapping("/blogs/{blogId}/comments/{commentId}")
    public ResponseEntity<?> deleteComment(@PathVariable int blogId,@PathVariable int commentId,Authentication authentication) {
        if(blogId < 1) {
            throw new IllegalArgumentException(Messages.INVALID_BLOG_ID);
        }
        Blog blog = blogService.findBlogByIdAndIsPublishedTrue(blogId);
        if(blog == null) {
            throw new EntityNotFoundException(Messages.BLOG_NOT_FOUND+blogId);
        }
        if(commentId < 1) {
            throw new IllegalArgumentException(Messages.INVALID_COMMENT_ID);
        }
        Comment commentStored = commentService.findCommentById(commentId);
        if(commentStored == null) {
            throw new EntityNotFoundException(Messages.COMMENT_NOT_FOUND+commentId);
        }
        User commentAuthor = commentStored.getUser();
        if(authentication != null && authentication.getName().equals(commentAuthor.getUsername()) || authentication.getAuthorities().toString().contains("ROLE_ADMIN")) {
            commentService.deleteById(commentId);
        }
        else{
            return ResponseEntity.status(403).body(Messages.UNAUTHORIZED_ACCESS);
        }
        return ResponseEntity.status(204).body("COMMENT DELETED SUCCESSFULLY.");
    }



}
